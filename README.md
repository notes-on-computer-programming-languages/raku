# raku

Was perl6. https://raku.org

[[_TOC_]]

# Official documentation
* [*Haskell to Raku - nutshell*
  ](https://docs.raku.org/language/haskell-to-p6)

# Unofficial documentation
* [*Learn X in Y minutes, Where X=Raku*
  ](https://learnxinyminutes.com/docs/raku/)
* [*Learn X in Y minutes, Where X=perl6*
  ](https://learnxinyminutes.com/docs/perl6/)
* [*Raku_(programming_language)*
  ](https://en.m.wikipedia.org/wiki/Raku_(programming_language))
* [*Raku*
  ](https://rosettacode.org/wiki/Raku)
* [*Functional Programming with Raku*
  ][F2023F] 2023-04 Rawley Fowler (DEV)
* [*Day 2: Perl is dead. Long live Perl and Raku*
  ](https://raku-advent.blog/2020/12/02/day-1-perl-is-dead-long-live-perl-and-raku/)
  2020-12 nigehamilton
* [*Day 19 – Functional Programming with Raku*][scimon2019-12FunctionalProgramming]
  2019-12 scimon (Raku Advent Calendar)

[F2023F]: https://dev.to/rawleyfowler/functional-programming-with-raku-9ib (Raku is a great language for functional programming, it has support for anonymous functions or blocks, partial application via assuming, and much more.)

[scimon2019-12FunctionalProgramming]:
  https://raku-advent.blog/2019/12/19/day-19-functional-programming-with-raku
  (I watched a really nice video recently, Functional Programming in 40 minutes, which I thought was really good. I’ve done bits of functional programming over the years but I know lots of people find the ideas behind it quite confusing.)

## Books
* [*Raku Recipes: A Problem-Solution Approach*][Merelo2020Recipes]
  2020-10 [1st ed.] J.J. Merelo (Apress)
* *Raku Fundamentals: A Primer with Examples, Projects, and Case Studies*
  2020-09 Moritz Lenz (Apress)

[Merelo2020Recipes]:
  https://ebin.pub/raku-recipes-a-problem-solution-approach-1st-ed-9781484262573-9781484262580.html

### Functional programming
* [*Think Raku: How to think like a computer scientist*
  ][Rosenfeld2020ThinkRaku]
  2020 (2nd Edition, Version 0.6)
  Laurent Rosenfeld and Allen B. Downey
  * Chapter 14. Functional Programming in Raku
* [*Think Perl 6: How to think like a computer scientist*
  ](https://www.worldcat.org/search?q=ti%3AThink+Perl+6)
  2017 Laurent Rosenfeld and Allen B. Downey
  * Chapter 14. Functional Programming in Perl
    * https://www.oreilly.com/library/view/think-perl-6/9781491980545/ch14.html
    * https://greenteapress.com/thinkperl6/html/thinkperl6015.html
* [*Perl 6 deep dive : data manipulation, concurrency, functional programming, and more*
  ](https://www.worldcat.org/search?q=ti%3APerl+6+Deep+Dive)
  2017 Andrew Shitov

[Rosenfeld2020ThinkRaku]:
  https://raw.githubusercontent.com/LaurentRosenfeld/think_raku/master/PDF/think_raku.pdf
  (This book is intended for beginners and does not require any prior programming knowledge, but it is my hope that even those of you with programming experience will benefit from reading it)

### Authors
* [*Andrew Shitov*
  ](https://andrewshitov.com/)

# Implementation: rakudo
* [*Rakudo*](https://en.m.wikipedia.org/wiki/Rakudo)

# Compilers: Perlito
* *"Perlito5" Perl to Java compiler and Perl to JavaScript compiler* [fglock/Perlito](https://github.com/fglock/Perlito)

# Features
## Algebraic Data Types
* [*Roles as Algebraic Data Types in Raku*
  ](https://wimvanderbauwhede.github.io/articles/roles-as-adts-in-raku/)

## Memory management
* [perl6 Resource acquisition is initialization (RAII)
  ](https://google.com/search?q=perl6+Resource+acquisition+is+initialization+%28RAII%29)

## Operators
* [*Creating operators*][raku.org:operators]

[raku.org:operators]:
  https://docs.raku.org/language/optut
  (A short tutorial on how to declare operators and create new ones.)

# Development tools
* zef package manager (slow because of compilation)
* [rakubrew](https://rakubrew.org/) installation
tool

# Packages
* [modules.raku.org](https://modules.raku.org)
* tag: [WEB](https://modules.raku.org/t/WEB)

## Web application frameworks or libraries/
### FastCGI::NativeCall

### Cro
* Set of libraries for building reactive distributed systems
* May have been inspirated by Perl Mojolicious.
* https://cro.services/docs/intro/getstarted
* Includes WebSocket.

### Bailador
* [*Web Application Development in Perl 6*](https://leanpub.com/bailador)
  * Free sample

### Crust
* Perl6 Superglue for Web frameworks and Web Servers

## String templates (web)
* [*Day 8: Raku web templating engines: boost up the parsing performance*
  ](https://raku-advent.blog/2020/12/08/raku-web-templating-engines-boost-up-the-parsing-performance/)
  2020-12 pheix

## Parser combinators
* [*Day 14: Writing Faster Raku code, Part I*
  ](https://raku-advent.blog/2020/12/14/day-15-writing-faster-raku-code-part-i/)
  2020-12 Wim Vanderbauwhede
* [*List-based parser combinators in Haskell and Raku*
  ](https://wimvanderbauwhede.github.io/articles/list-based-parser-combinators/)
  2020-06 Wim Vanderbauwhede
* [wimvanderbauwhede/list-based-combinators-raku](https://github.com/wimvanderbauwhede/list-based-combinators-raku)
